const inventory = require('../inventory.cjs')

const findCarById = require('../problem1.cjs');


const result = findCarById(inventory, 33);


let carMake = result.car_make
let carModel = result.car_model
let carYear =result.car_year
let carId = result.id
console.log(`Car ${carId} is a ${carYear} ${carMake} ${carModel}`)
