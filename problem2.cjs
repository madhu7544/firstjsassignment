function problem2(inventory){
    let last = inventory.length-1
    let lastCar = inventory[last]
    let carMake = lastCar.car_make
    let carModel =lastCar.car_model
    return `Last car is a ${carMake} ${carModel}`
}

module.exports = problem2
