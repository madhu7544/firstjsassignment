// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function problem4(inventory){
    let allCarYears = []
    for (let index=0;index<inventory.length;index++){
        let carYear = inventory[index].car_year
        allCarYears.push(carYear)
    }
    // return all car years
    return allCarYears
}

module.exports= problem4