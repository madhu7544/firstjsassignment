function problem3(inventory){
    let carModelsList = [];
    for (let index=0;index<inventory.length;index++){
        let carModel = inventory[index].car_model
        carModelsList.push(carModel)
    }
    // Here i push the carModel to carModelsList by using for loop

    carModelsList.sort()
    // By using sort method i arranged Alphabetically
    return carModelsList
 }

 
 module.exports = problem3