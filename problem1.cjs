function findCarById(inventory,carId){
    if (carId===null || carId=== undefined || carId==='' || !inventory.length || !inventory){
        return [];
    }else if(!Array.isArray(inventory)){
        return [];
    }else if (typeof(carId) !=="number"){
        return [];
    }
    else{
        for(let index=0; index<inventory.length;index++){
            if (inventory[index].id==carId){
                return inventory[index];
            }
        }
}
}


module.exports= findCarById
