// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


function problem6(inventory){
    let bmwAndAudi = [];
    for (let index=0;index<inventory.length;index++){
        let carMake = inventory[index].car_make
        if (carMake ==="BMW" || carMake === "Audi"){
            bmwAndAudi.push(inventory[index])
        }
    }

    // return json data of bmw and audi cars 
    return JSON.stringify(bmwAndAudi)
}

module.exports = problem6