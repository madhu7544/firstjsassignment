//The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

let inventory = require('./inventory.cjs')
const allCarYears = require('./problem4.cjs')

const carYears = allCarYears(inventory)

const olderCars =[]
function problem5(){
    for(let index=0;index<carYears.length;index++){
        let year = carYears[index]
        if (year < 2000){
            olderCars.push(inventory[index].car_model)
        }
    }

    return olderCars
}

module.exports =problem5()


